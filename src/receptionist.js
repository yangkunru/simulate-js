// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.
// <-start-
class Receptionist {
  constructor () {
    this.nameList = [];
  }

  register (person) {
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    if (!(person instanceof Object) || (person === null) || (person === undefined) ||
    Object.entries(person).length === 0 || !hasOwnProperty.call(person, 'id') || !hasOwnProperty.call(person, 'name')) {
      throw new Error('Invalid person');
    }

    this.nameList.forEach(value => {
      if (value.id === person.id) {
        throw new Error('Person already exist');
      }
    });
    this.nameList.push(person);
    return true;
  }

  getPerson (id) {
    let person;
    this.nameList.forEach(value => {
      if (value.id === id) {
        person = value;
      }
    });
    if (person === undefined) {
      throw new Error('Person not found');
    }
    return person;

  }
}
// --end-->

export default Receptionist;
