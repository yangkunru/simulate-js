// TODO: Write a simple function called `greeting`.
//
// This function accepts a string argument and returns a new string.
// The content returned should be "Hello" plus the argument.
//
// For example. If the argument is "World". Then the returned string
// should be "Hello World".
// <--start-
function greeting (str) {
  return 'Hello ' + str;
}
// ---end->

export default greeting;
