// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (arrayA, arrayB) {
  if (arrayA.length === 0 || arrayB.length === 0) {
    return [];
  }
  let arraySize = 0;
  if (arrayA.length >= arrayB.length) {
    arraySize = arrayB.length;
  } else {
    arraySize = arrayA.length;
  }
  const finalArray = [];
  for (let i = 0; i < arraySize; i++) {
    const elementArray = [];
    elementArray.push(arrayA[i]);
    elementArray.push(arrayB[i]);
    finalArray.push(elementArray);
  }
  return finalArray;
}
// --end->

export default zip;
