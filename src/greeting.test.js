import greeting from './greeting';

describe('when calling greeting', () => {
  fit('should return the combined greeting', () => {
    const words = greeting('World');
    expect(words).toStrictEqual('Hello World');
  });
});
